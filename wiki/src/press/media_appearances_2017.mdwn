[[!meta title="Media appearances in 2017"]]

* 2017-02-11:
  [UX et logiciels libres : retour d’expérience (TAILS)](http://romy.tetue.net/ux-et-logiciels-libres-retour-d-experience-tails)
  by Romy Duhem-Verdière (aka tetue), about the report talk we gave at
  Pas Sage En Seine on 2015-06-18, with the designers who helped us
  make it easier to start using Tails (in French).
